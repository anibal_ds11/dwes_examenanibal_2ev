<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Pintor extends Model
{
	protected $table='pintores';

	public function getCuadros()
    {
    	return $this->hasMany('App\Cuadro','pintor_id','id');
    }
}
