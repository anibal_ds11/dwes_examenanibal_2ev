<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuadro;
use App\Pintor;
class CuadrosController extends Controller
{

	 public function getCrear()
    {
    	return view('cuadros.crear',array('arraypintores'=>Pintor::all()));
    }
    public function postCrear(Request $request)
    {
        $cuadro = new Cuadro();
        $cuadro->nombre = $request->nombre;
        $pintor=Pintor::where('nombre',$request->nombre)->firstOrFail();

        $cuadro->pintor_id = $pintor->id;
        $cuadro->imagen = $request->imagen->store('','cuadros');
        try{
            $cuadro->save();
            return redirect('pintors')->with('mensaje','cuadro insertada');
        }catch(\Illuminate\Database\QueryException $ex){
            return redirect('mascotas')->with('mensaje','Fallo al crear el cuadro');
        }
    }
}