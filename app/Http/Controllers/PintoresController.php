<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pintor;
use App\Cuadro;

class PintoresController extends Controller
{
   public function getTodos()
    {
    	return view("pintores.index",array('arraypintores'=>Pintor::all()));
    }
     public function getMostrar($id)
    {
        return view('pintores.mostrar',array('pintor'=>Pintor::findOrFail($id),'cuadros'=>Cuadro::where('pintor_id',$id)->get()));
    }
    
}
