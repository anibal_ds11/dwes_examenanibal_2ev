@extends("layouts.master")

@section("titulo")
Pintores
@endsection
@section("contenido") 
@if(session('mensaje'))
	<div class="alert alert-danger">
		{{ session('mensaje')}}
	</div>
@endif
<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 ">
					<table width="100%">
					<tr>
					<th>Nombre</th>
					<th>Pais</th>
					<th>Cuadros</th>
					</tr>
			@foreach( $arraypintores as $clave => $pintor )
				<tr>
					<td><a href="{{ url('/pintores/mostrar/' . $pintor->id) }}">{{$pintor->nombre}}</a></td>
					<td>{{$pintor->pais}}</td>
					<td>{{$pintor->getCuadros()->count()}}</td>
				</tr>
				@endforeach
		</table>
		</div>
		</div>
</div>

@endsection