@extends("layouts.master")

@section("titulo")
Pintores
@endsection
@section("contenido") 
<div class="row">
<div class="col-sm-9">
<h3>{{$pintor->nombre}}</h3>
<h4>Pais: {{$pintor->pais}}</h4>
<h4>Cuadros</h4>
@foreach( $cuadros as $cuadro)

<h5>{{$cuadro->nombre}}</h5>
<img src="{{asset('assets/imagenes')}}/{{$cuadro->imagen}}" style="height:200px"/>

@endforeach
</div>
</div>

@endsection
